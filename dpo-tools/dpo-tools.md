dpo-tools

O dpo-tools é um conjunto de shell scripts que podem auxiliar na tradução
 de arquivos podebconf do Debian.

Funcionalidades

 * faz o download de um arquivo templates.pot a partir do repositório de templates podebconf
 * cria e inicializa um arquivo podebconf a partir de um arquivo de templates
 * atualiza um arquivo podebconf com uma nova versão de seu arquivo de templates
 * atualiza um arquivo podebconf com strings já traduzidas, a partir de um arquivo compendium
 * verifica se o arquivo podebconf está bem formado
 * apresenta informações no formato diff de forma colorida e paginada
 * cria um arquivo de patch baseado em duas versões de um arquivo podebconf
 * quebra as linhas de um arquivo podebconf na coluna 80

Licença

 O dpo-tools é software livre licenciado sob a GPLv3 ou posterior.

Código fonte

 [dpo-tools_1.2.tgz](https://salsa.debian.org/debian/l10n-br/blob/master/dpo-tools/dpo-tools_1.2.tgz)
 [dpo-tools_1.2.tgz.asc](https://salsa.debian.org/debian/l10n-br/blob/master/dpo-tools/dpo-tools_1.2.tgz.asc)

Documentação

 [Manual do usuário do dpo-tools](https://www.arg.eti.br/blog/pages/software/dpo-tools/manual-do-usuario-do-dpo-tools.html)


